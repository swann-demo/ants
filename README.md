# Example Apache Ant Application # 

This application allows the testing of building very simple ant application

## Docker running ##
1. `docker run -it --rm -v $(PWD):/project webratio/ant:1.10.1 bash`
1. `cd project`
1. `ant`

## Gitlab SAST ##
1. `docker run  -it --rm -v "$PWD":/code -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/gitlab-org/security-products/sast:12-10-stable /app/bin/run code`

## Spotbugs ##
1. `docker run -it --rm -v $(PWD):/project registry.gitlab.com/gitlab-org/security-products/analyzers/spotbugs:2 bash`
1. `./analyzer run --target-dir /project`
1. `cat gl-sast-report.json`